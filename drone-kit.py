import time, sched
import sys
import argparse
import threading
from datetime import datetime
import os
import platform
import errno


from pymavlink import mavutil
from dronekit import connect, VehicleMode, LocationGlobalRelative


STATUS_PRINT_FILE_LOG_TIMER = 0.5 #in seconds
STATUS_PRINT_CONSOLE_LOG_TIMER = 5 #in seconds
TARGET_ALTITUTE = 4  #in meters

is_finished_script = False #helps to stop a thread

parser = argparse.ArgumentParser()
parser.add_argument('--connect', default='/dev/ttyS0')
parser.add_argument('-x', default='0')
parser.add_argument('-y', default='0')
parser.add_argument('-z', default='0')
args = parser.parse_args()

# Connect to the Vehicle
print 'Connecting to vehicle on: %s' % args.connect
vehicle = connect(args.connect, baud=57600, wait_ready=True)

try:
    os.makedirs('logs') # Checks if the directory exists, if not creates one
except OSError as e:
    if e.errno != errno.EEXIST:
        raise

file_path = os.path.join('logs', str(datetime.now())[:-10] + "-data.csv")

def write_log(log_line):
    with open(file_path, "a") as file:
        file.write(log_line + '\n')


parameters_names = [
    'time', 'pitch, yaw, roll',
    'Global Location lat, lon, alt',
    'Relative alt',
    'Local Location north, east, down',
    'velocity_x, velocity_y, velocity_z',
    'Groundspeed', 'Airspeed',
    'Battery voltage, Battery current, Battery level',
    'Heading',
    'GPS fix type', 'GPS satellites_visible',
    'EKF OK?', 'Is Armable?', 'System status', 'Mode', 'Armed', 'Autopilot Firmware version'
]

write_log(','.join(parameters_names))

s = sched.scheduler(time.time, time.sleep)
def vehicle_status():
    # stop logging
    if is_finished_script == True:
        return

    s.enter(STATUS_PRINT_FILE_LOG_TIMER, 1, vehicle_status, ())

    parameters_list = [
        str(datetime.now()),
        str(vehicle.attitude.pitch), str(vehicle.attitude.yaw), str(vehicle.attitude.roll),
        str(vehicle.location.global_frame.lat), str(vehicle.location.global_frame.lon), str(vehicle.location.global_frame.alt),
        str(vehicle.location.global_relative_frame.alt),
        str(vehicle.location.local_frame.north), str(vehicle.location.local_frame.east), str(vehicle.location.local_frame.down),
        str(vehicle.velocity[0]), str(vehicle.velocity[1]), str(vehicle.velocity[2]),
        str(vehicle.groundspeed), str(vehicle.airspeed),
        str(vehicle.battery.voltage), str(vehicle.battery.current), str(vehicle.battery.level),
        str(vehicle.heading),
        str(vehicle.gps_0.fix_type), str(vehicle.gps_0.satellites_visible),
        str(vehicle.ekf_ok), str(vehicle.is_armable), str(vehicle.system_status.state),
        str(vehicle.mode.name), str(vehicle.armed), str(vehicle.version)
    ]
    write_log(','.join(parameters_list))
    # print "========= VEHICLE STATUS =====================:"
    # print "Autopilot Firmware version: %s" % vehicle.version
    # print "Autopilot capabilities (supports ftp): %s" % vehicle.capabilities.ftp
    # print "Global Location: %s" % vehicle.location.global_frame
    # print "Global Location (relative altitude): %s" % vehicle.location.global_relative_frame
    # print "Local Location: %s" % vehicle.location.local_frame    #NED
    # print "Attitude: %s" % vehicle.attitude
    # print "Velocity: %s" % vehicle.velocity
    # print "GPS: %s" % vehicle.gps_0
    # print "Groundspeed: %s" % vehicle.groundspeed
    # print "Airspeed: %s" % vehicle.airspeed
    # print "Gimbal status: %s" % vehicle.gimbal
    # print "Battery: %s" % vehicle.battery
    # print "EKF OK?: %s" % vehicle.ekf_ok
    # print "Last Heartbeat: %s" % vehicle.last_heartbeat
    # print "Rangefinder: %s" % vehicle.rangefinder
    # print "Rangefinder distance: %s" % vehicle.rangefinder.distance
    # print "Rangefinder voltage: %s" % vehicle.rangefinder.voltage
    # print "Heading: %s" % vehicle.heading
    # print "Is Armable?: %s" % vehicle.is_armable
    # print "System status: %s" % vehicle.system_status.state
    # print "Mode: %s" % vehicle.mode.name    # settable
    # print "Armed: %s" % vehicle.armed    # settable
    # #print " Threads count: %s" % threading.active_count()
    #print "================================================"

def vehicle_status_repeated():
    # s = sched.scheduler(time.time, time.sleep)
    s.enter(STATUS_PRINT_FILE_LOG_TIMER, 1, vehicle_status, ())
    s.run()

def init_vehicle():
    # if vehicle.mode.name == "INITIALISING":
    #  print "Waiting for vehicle to initialise"
    #  time.sleep(1)

    #while vehicle.gps_0.fix_type < 2:
    #   print "Waiting for GPS...:", vehicle.gps_0.fix_type
    #  time.sleep(1)

    # Don't let the user try to arm until autopilot is ready
    # while not vehicle.is_armable:
    #  print " Waiting for vehicle to be armable..."
    # time.sleep(1)

    for i in xrange(3,0,-1):
        print "Arming motors in %s seconds" % i
        time.sleep(1)

    # Copter should arm in GUIDED mode
    vehicle.mode    = VehicleMode("GUIDED")
    vehicle.armed   = True

    while not vehicle.armed:
        print " Waiting for arming..."
        time.sleep(1)

    print " Vehicle is armed"

    print "Now waiting for 5 seconds"
    for i in xrange(5,0,-1):
        print "%s seconds" % i
        time.sleep(1)

# Function to arm and then takeoff to a user specified altitude
def arm_and_takeoff(aTargetAltitude):
    init_vehicle()

    print "Taking off!"
    vehicle.simple_takeoff(aTargetAltitude) # Take off to target altitude

    # Check that vehicle has reached takeoff altitude
    while True:
        print " Altitude: ", vehicle.location.global_relative_frame.alt
        #Break and return from function just below target altitude.
        if vehicle.location.global_relative_frame.alt>=aTargetAltitude*0.95:
            print "========================"
            print "Reached target altitude"
            print "========================"
            break
        time.sleep(1)

#print status each STATUS_PRINT_TIMER seconds
threading.Thread(target=vehicle_status_repeated).start()
# vehicle_status_repeated()

# Initialize the takeoff sequence
arm_and_takeoff(TARGET_ALTITUTE)


print("Take off completed")

print "Now waiting for 7  seconds"
for i in xrange(7,0,-1):
    print "%s seconds" % i
    print " Altitude: ", vehicle.location.global_relative_frame.alt
    time.sleep(1)

print "set the default/travel speed to 3"
vehicle.airspeed=3

print "Going towars first point for 30 seconds..."


point1 = LocationGlobalRelative(args.x , args.y, args.z)
vehicle.simple_goto(point1)



# sleep so we can see the change in map
time.sleep(30)


print("SWITCHING TO RTL")
vehicle.mode = VehicleMode("RTL")
vehicle.flush()

# Close vehicle object
vehicle.close()

# stop log thread
is_finished_script = True
